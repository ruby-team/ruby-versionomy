ruby-versionomy (0.5.0-3) unstable; urgency=medium

  [ Ben Armstrong ]
  * Removed myself from Uploaders.

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Use secure URI in debian/watch.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 9 to 12.
  * Remove constraints unnecessary since buster

  [ Jenkins ]
  * Update watch file format version to 4.
  * Bump debhelper from old 12 to 13.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.

  [ Cédric Boutillier ]
  * Use https:// in Vcs-* fields
  * Move debian/watch to gemwatch.debian.net
  * Bump Standards-Version to 4.6.1 (no changes needed)
  * Use gem install layout
  * Add upstream metadata
  * Use the rake method to run tests
  * Refresh packaging with dh-make-ruby -w
    + Add myself to Uploaders
    + Update homepage URL
    + Use gem install layout
    + Drop explicit dependency on ruby interpreter
  * Use unsafe_load to load YAML in tests (Closes: #1019675)
  * Stop moving around version file, as this issue is fixed with the gem
    layout

 -- Cédric Boutillier <boutil@debian.org>  Tue, 22 Nov 2022 22:43:10 +0100

ruby-versionomy (0.5.0-2) unstable; urgency=medium

  * Team upload.

  [ Thiago Ribeiro ]
  * Imported Upstream version 0.5.0
  * Bump compat to 9

  [ Christian Hofstaedtler ]
  * Update packaging using dh-make-ruby -w

 -- Christian Hofstaedtler <zeha@debian.org>  Tue, 01 Mar 2016 13:29:13 +0100

ruby-versionomy (0.4.4-1) unstable; urgency=low

  * Initial release (Closes: #719314)

 -- Ben Armstrong <synrg@sanctuary.nslug.ns.ca>  Sun, 11 Aug 2013 08:41:58 -0300
